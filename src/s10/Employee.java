package s10;
import java.text.DecimalFormat;
public class Employee {
    private int id;
    private String firstName;
    private String lastName;
    private int salary;
    public Employee(int id, String firstName, String lastName, int salary) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.salary = salary;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public int getSalary() {
        return salary;
    }
    public void setSalary(int salary) {
        this.salary = salary;
    }
    public String getName(){
        return firstName + " " + lastName;
    }
    public int getAnnualSalary(){
        return salary * 12;
    }
    public String raiseSalary(float percent){
        float newSalary = salary * (1 + percent/100);
        //Định dạng tiền
        DecimalFormat df = new DecimalFormat("###,###.##"); // định dạng số
        String formatedNewSalary = df.format(newSalary);
        return formatedNewSalary;
    }
    @Override
    public String toString() {
        return "Employee [id=" + id 
        + ", name=" + firstName + " " + lastName 
        + ", salary=" + salary
                + "]";
    }
    
}
